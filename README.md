# UW Alma Mashup Skins #

These are our Alma mashup skins for use at UW. Unlike with the previous Primo UI, we do not specify a skin with the PBO. Instead, each Primo view X uses the mashup skin named X. In general, we only need a mashup skin named *UW*.

Our CSS adds coloring and spacing to the pages as well as hides unwatned features. 

At the top of the file are `body:before` and `body:after` pseudoselectors. If you change their values to what's commented there, you get visual indicators of what is in an Alma iframe. UW_NEW is setup for this currently. The UW skin is not.

## Testing Advice ##

After every Alma update, it's advisable to check for changes. To do this, one should do a search for the following types of items to ensure coverage:

* A physical item owned by the UW that is single-part (e.g., Harry Potter book or DVD)

* A physical item owned by the UW with multiple parts (e.g., Vogue)

* A Summit item (e.g., Scars of Dyslexia)

* An electronic article, ideally one with related titles (e.g., several items for autism genetics)

* An electronic item that we can get a scan of

Be sure to also check the request forms for both Summit and local requests.
